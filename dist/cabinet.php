<?php require_once("header.php"); ?>
<div class="cabinet">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h3 class="cabinet__title">
                    Личный кабинет
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="nav flex-column cabinet__navs-list" id="v-pills-tab" role="tablist"
                    aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab"
                        aria-controls="v-pills-home" aria-selected="true">Home</a>
                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab"
                        aria-controls="v-pills-profile" aria-selected="false">Profile</a>
                    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab"
                        aria-controls="v-pills-messages" aria-selected="false">Messages</a>
                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab"
                        aria-controls="v-pills-settings" aria-selected="false">Settings</a>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="tab-content cabinet__navs-body" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                        aria-labelledby="v-pills-home-tab">
                        <div class="row">
                            <div class="col-sm-6 tab-content-left">
                                
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 mb-4">
                                        <h2>Личные данные</h2>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <p>Имя</p>
                                    </div>
                                    <div class="col-sm-12 col-md-8"><input type="text"></div>
                                    <div class="col-sm-12 col-md-4">
                                        <p>Фамилия</p>
                                    </div>
                                    <div class="col-sm-12 col-md-8"><input type="text"></div>
                                    <div class="col-sm-12 col-md-4">
                                        <p>Дата рождения</p>
                                    </div>
                                    <div class="col-sm-12 col-md-8 flexis date-of-birth">
                                        <select class="date js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                          
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>
                                          <option value="5">5</option>
                                          <option value="6">6</option>
                                          <option value="7">7</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>
                                          <option value="21">21</option>
                                          <option value="22">22</option>
                                          <option value="23">23</option>
                                          <option value="24">24</option>
                                          <option value="25">25</option>
                                          <option value="26">26</option>
                                          <option value="27">27</option>
                                          <option value="28">28</option>
                                          <option value="29">29</option>
                                          <option value="30">30</option>
                                          <option value="31">30</option>
                                        </select>
                                        <select class="date js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                          
                                          <option value="янв">янв</option>
                                          <option value="фев">фев</option>
                                          <option value="мар">мар</option>
                                          <option value="апр">апр</option>
                                          <option value="май">май</option>
                                          <option value="июнь">июнь</option>
                                          <option value="июль">июль</option>
                                          <option value="авг">авг</option>
                                          <option value="сен">сен</option>
                                          <option value="окт">окт</option>
                                          <option value="нояб">нояб</option>
                                          <option value="дек">дек</option>
                                        </select>
                                        <select class="date js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                          
                                          <option value="1980">1980</option>
                                          <option value="1981">1981</option>
                                          <option value="1982">1982</option>
                                          <option value="1983">1983</option>
                                          <option value="1984">1984</option>
                                          <option value="1985">1985</option>
                                          <option value="1986">1986</option>
                                          <option value="1987">1987</option>
                                          <option value="1988">1988</option>
                                          
                                          <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <p>Телефон</p>
                                    </div>
                                    <div class="col-sm-12 col-md-8"><input type="text"></div>
                                    <div class="col-sm-12 col-md-4">
                                        <p>Пароль</p>
                                    </div>
                                    <div class="col-sm-12 col-md-8"><input type="text"></div>
                                    <div class="col-sm-12 col-md-4">
                                        <p>Размер одежды</p>
                                    </div>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="clothing-sizes js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                          
                                          <option value="S">S(37-38)</option>
                                          <option value="M">M (39-40)</option>
                                          <option value="L">L (41-42)</option>
                                          <option value="XL">XL (43-44)</option>
                                          <option value="XXL">XXL (45-46)</option>
                                          
                                          
                                          <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <p>Размер обуви</p>
                                    </div>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="clothing-sizes js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">   
                                          <option value="S">37</option>
                                          <option value="M">38</option>
                                          <option value="L">39</option>
                                          <option value="XL">40</option>
                                          <option value="XXL">41</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <?php require("cabinet-address.component.php"); ?>
                                <?php require("cabinet-address.component.php"); ?>
                                <div class="row">
                                    <div class="col-sm-12 mt-5">
                                        <div class="add-address">
                                            <span class="add-address-btn">ДОБАВИТЬ АДРЕС</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-4"></div>
                                    <div class="col-sm-8 mt-5 mb-5">
                                        <div class="save-address">
                                            <input type="submit" value="Сохранить">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel"
                        aria-labelledby="v-pills-profile-tab">2</div>
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
                        aria-labelledby="v-pills-messages-tab">3</div>
                    <div class="tab-pane fade" id="v-pills-settings" role="tabpanel"
                        aria-labelledby="v-pills-settings-tab">4</div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php require_once("footer.php"); ?>