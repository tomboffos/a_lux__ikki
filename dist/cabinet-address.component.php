<div class="address-item">
    <div class="row">
        <div class="col-sm-12 col-md-12 mb-4">
            <h2>Адрес доставки</h2>
        </div>
        <div class="col-sm-12 col-md-4">
            <p>Город</p>
        </div>
        <div class="col-sm-12 col-md-8"><input type="text"></div>
        <div class="col-sm-12 col-md-4">
            <p>Улица</p>
        </div>
        <div class="col-sm-12 col-md-8"><input type="text"></div>
        <div class="col-sm-12 col-md-4">

        </div>
        <div class="col-sm-6 col-md-4">
            <p>Дом</p>
            <select class="homenumber js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
              
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              
              <option value="4">4</option>
            </select>
        </div>
        <div class="col-sm-6 col-md-4">
            <p>Квартира/ офис</p>
            <select class="apartment js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
        </div>
        <div class="col-sm-12 col-md-4">

        </div>
        <div class="col-sm-6 col-md-4">
            <p>Подъзд</p>
            <select class="porch js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
        </div>
        <div class="col-sm-6 col-md-4">
            <p>Этаж</p>
            <select class="floor js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
        </div>

    </div>

</div>