<?php require_once("header.php"); ?>
<?php require_once("breadCrumbs.php"); ?>

<div class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="contacts__title">
                Магазины
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="contacts__map-container">

                </div>
            </div>
            <div class="col-sm-4">
                <div class="contacts__column--wrapper">
                    <div class="contacts__column--wrapper__item">
                        <h2>
                            Наш Адрес
                        </h2>
                        <p><span>Казахстан, Г. Алма-Ата, Набережная 19</span></p>
                    </div>
                    <div class="contacts__column--wrapper__item">
                        <h2>
                            Наш телефон
                        </h2>
                        <p><span> <a href=""> 8 800 321 50 14 </a>/ <a href="">+7 (961) 525 79 31</a></span></p>
                    </div>
                    <div class="contacts__column--wrapper__item">
                        <h2>
                            Наша почта
                        </h2>
                        <a href="">ikkiinfo@mail.com</a>
                    </div>
                </div>
                <a href="#" data-toggle="modal" data-target="#exampleModalCenter"
                    class=" contacts__btn-feddback">Написать нам</a>
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">

                            <div class="modal-body">
                                <input type="text" placeholder="ФИО">
                                <input type="text" placeholder="Email">
                                <input type="text" placeholder="Телефон">
                                <textarea name="" id="" cols="30" rows="5"></textarea>
                            </div>
                            <div class="modal-footer">

                                <div class="col-sm-6">
                                    <button class="contacts__btn-feddback contacts__btn-feddback--modal" data-dismiss="modal">Закрыть</button>
                                </div>
                                <div class="col-sm-6">
                                    <button class="contacts__btn-feddback contacts__btn-feddback--modal">Отправить</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once("slider.php"); ?>
<?php require_once("footer.php"); ?>