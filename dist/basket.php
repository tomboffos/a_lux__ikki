<?php require_once("header.php"); ?>
<?php require_once("breadCrumbs.php"); ?>
<div class="basket">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="basket__title">
                    Корзина
                </h3>
            </div>
        </div>
        <div class="basket-wrapper">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="product-basket">
                        <img src="images/basket1.png" alt="basket1.png">
                                <div class="product-basket__column">
                            <p>ТОВАР</p>
                            <p>ЮБКА МИДИ ИЗ ШЁЛКА
                                НА РЕЗИНКЕ</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-2">
                    <div class="product-basket__column">
                        <p>РАЗМЕР</p>
                        <p></p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-2">
                    <div class="product-basket__column">
                        <p>ЦВЕТ</p>
                        <div class="color-preference">
                            <span>
                                <a></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-2">
                    <div class="product-basket__column">
                        <p>КОЛ-ВО</p>
                        <div class="number-input operations-block">
                            <span onclick="minusAmount(768)" class="btn-operation">-</span>
                            <input class="quantity text-center  input-text" disabled="" min="1" id="inputAmount768"
                                name="count" value="1" type="number">
                            <span onclick="plusAmount(768)" class="btn-operation">+</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-2">
                    <div class="product-basket__column">
                        <?php require("./components/basketSvg.component.php"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="basket-wrapper">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="product-basket">
                        <img src="images/basket1.png" alt="basket1.png">
                        <div class="product-basket__column">
                            <p>ТОВАР</p>
                            <p>ЮБКА МИДИ ИЗ ШЁЛКА
                                НА РЕЗИНКЕ</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-2">
                    <div class="product-basket__column">
                        <p>РАЗМЕР</p>
                        <p></p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-2">
                    <div class="product-basket__column">
                        <p>ЦВЕТ</p>
                        <div class="color-preference">
                            <span>
                                <a></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-2">
                    <div class="product-basket__column">
                        <p>КОЛ-ВО</p>
                        <div class="number-input operations-block">
                            <span onclick="minusAmount(768)" class="btn-operation">-</span>
                            <input class="quantity text-center  input-text" disabled="" min="1" id="inputAmount768"
                                name="count" value="1" type="number">
                            <span onclick="plusAmount(768)" class="btn-operation">+</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-2">
                    <div class="product-basket__column">
                        <?php require("./components/basketSvg.component.php"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="basket-clear">
            <div class="row">
                <div class="col-sm-3">
                    <p id="clearBasketEvent">
                        ОЧИСТИТЬ КОРЗИНУ
                    </p>
                </div>
            </div>
        </div>

        <div class="submit-order">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="basket__submit-title">
                        Оформление заказа
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="submit-order__field">
                        <p>E-mail</p>
                        <input type="text" placeholder="example@mail.ru">
                    </div>
                    <div class="submit-order__field">
                        <p>Имя</p>
                        <input type="text" placeholder="Карина">
                    </div>
                    <div class="submit-order__field">
                        <p>Фамилия</p>
                        <input type="text" placeholder="Исмаилова">
                    </div>
                    <div class="submit-order__field">
                        <p>Телефон</p>
                        <input type="text" placeholder="+7 (777) 366 66 91">
                    </div>
                    <div class="submit-order__field">
                        <p>Населённый пункт</p>
                        <select class="js-example-basic-single" name="state">
                            <option value="ALA">Алматинская обл.</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="submit-order__field submit-order__field__textarea ">
                        <p>Комментарий</p>
                        <textarea name="" id="" cols="30" rows="3" placeholder="Введите ваше сообщение"></textarea>
                    </div>
                </div>
                <div class="col-sm-3">

                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once("footer.php"); ?>