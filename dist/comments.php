<?php require_once("header.php"); ?>
<?php require_once("breadCrumbs.php"); ?>
<div class="comments">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="comments__title">
                    Справочник
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="comment__item">
                    <span class="comment__item__rate"></span>
                    <h3 class="comment__item__title">Отличный магазин. Обращусь еще</h3>
                    <p class="comment__item__text">Таким образом постоянное информационно-пропагандистское обеспечение
                        нашей деятельности способствует подготовки и реализации существенных финансовых и
                        административных условий. Разнообразный и богатый опыт новая модель организационной деятельности
                        влечет за собой процесс внедрения и модернизации соответствующий условий активизации.

                        Значимость этих проблем настолько очевидна, что рамки и место обучения кадров в значительной
                        степени обуславливает создание форм развития. С другой стороны укрепление и развитие структуры
                        обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного
                        развития.

                        Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную
                        роль в формировании соответствующий условий активизации. Значимость этих проблем настолько
                        очевидна, что дальнейшее развитие различных форм деятельности в значительной степени
                        обуславливает создание позиций, занимаемых участниками в отношении поставленных задач.

                        Разнообразный и богатый опыт новая модель организационной деятельности требуют от нас анализа
                        новых предложений. С другой стороны реализация намеченных плановых заданий в значительной
                        степени обуславливает создание соответствующий условий активизации. Не следует, однако забывать,
                        что рамки и место обучения кадров позволяет оценить значение модели развития.</p>
                    <div class="comment__item__name-date--wrapper">
                        <p>Александр, 23 года</p>
                        <p> <span> 23 декабря, 2018</span></p>
                    </div>
                </div>
                <div class="comment__item">
                    <span class="comment__item__rate" id="starRatingComments">
                            <svg id='star1' xmlns="http://www.w3.org/2000/svg" width="10px" height="10px" viewBox="0 0 576 512"><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/></svg>
                            <svg id='star2' xmlns="http://www.w3.org/2000/svg" width="10px" height="10px" viewBox="0 0 576 512"><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/></svg>
                            <svg id='star3' xmlns="http://www.w3.org/2000/svg" width="10px" height="10px" viewBox="0 0 576 512"><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/></svg>
                            <svg id='star4' xmlns="http://www.w3.org/2000/svg" width="10px" height="10px" viewBox="0 0 576 512"><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/></svg>
                            <svg id='star5' xmlns="http://www.w3.org/2000/svg" width="10px" height="10px" viewBox="0 0 576 512"><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/></svg>
                    </span>
                    <h3 class="comment__item__title">Большой ассортимент одежды</h3>
                    <p class="comment__item__text">Таким образом постоянное информационно-пропагандистское обеспечение
                        нашей деятельности способствует подготовки и реализации существенных финансовых и
                        административных условий. Разнообразный и богатый опыт новая модель организационной деятельности
                        влечет за собой процесс внедрения и модернизации соответствующий условий активизации.

                        Значимость этих проблем настолько очевидна, что рамки и место обучения кадров в значительной
                        степени обуславливает создание форм развития. С другой стороны укрепление и развитие структуры
                        обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного
                        развития.

                        Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную
                        роль в формировании соответствующий условий активизации. Значимость этих проблем настолько
                        очевидна, что дальнейшее развитие различных форм деятельности в значительной степени
                        обуславливает создание позиций, занимаемых участниками в отношении поставленных задач.

                        Разнообразный и богатый опыт новая модель организационной деятельности требуют от нас анализа
                        новых предложений. С другой стороны реализация намеченных плановых заданий в значительной
                        степени обуславливает создание соответствующий условий активизации. Не следует, однако забывать,
                        что рамки и место обучения кадров позволяет оценить значение модели развития.</p>
                    <div class="comment__item__name-date--wrapper">
                        <p>Александр, 23 года</p>
                        <p> <span> 23 декабря, 2018</span></p>
                    </div>
                </div>
                <div class="comment__item">
                    <span class="comment__item__rate"></span>
                    <h3 class="comment__item__title">Это просто невероятно. Обязательно всем рекомендую ikki</h3>
                    <p class="comment__item__text">Таким образом постоянное информационно-пропагандистское обеспечение
                        нашей деятельности способствует подготовки и реализации существенных финансовых и
                        административных условий. Разнообразный и богатый опыт новая модель организационной деятельности
                        влечет за собой процесс внедрения и модернизации соответствующий условий активизации.

                        Значимость этих проблем настолько очевидна, что рамки и место обучения кадров в значительной
                        степени обуславливает создание форм развития. С другой стороны укрепление и развитие структуры
                        обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного
                        развития.

                        Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную
                        роль в формировании соответствующий условий активизации. Значимость этих проблем настолько
                        очевидна, что дальнейшее развитие различных форм деятельности в значительной степени
                        обуславливает создание позиций, занимаемых участниками в отношении поставленных задач.

                        Разнообразный и богатый опыт новая модель организационной деятельности требуют от нас анализа
                        новых предложений. С другой стороны реализация намеченных плановых заданий в значительной
                        степени обуславливает создание соответствующий условий активизации. Не следует, однако забывать,
                        что рамки и место обучения кадров позволяет оценить значение модели развития.</p>
                    <div class="comment__item__name-date--wrapper">
                        <p>Александр, 23 года</p>
                        <p> <span> 23 декабря, 2018</span></p>
                    </div>
                </div>
                <div class="comment__item">
                    <span class="comment__item__rate"></span>
                    <h3 class="comment__item__title">Отличный магазин. Обращусь еще</h3>
                    <p class="comment__item__text">Таким образом постоянное информационно-пропагандистское обеспечение
                        нашей деятельности способствует подготовки и реализации существенных финансовых и
                        административных условий. Разнообразный и богатый опыт новая модель организационной деятельности
                        влечет за собой процесс внедрения и модернизации соответствующий условий активизации.

                        Значимость этих проблем настолько очевидна, что рамки и место обучения кадров в значительной
                        степени обуславливает создание форм развития. С другой стороны укрепление и развитие структуры
                        обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного
                        развития.

                        Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную
                        роль в формировании соответствующий условий активизации. Значимость этих проблем настолько
                        очевидна, что дальнейшее развитие различных форм деятельности в значительной степени
                        обуславливает создание позиций, занимаемых участниками в отношении поставленных задач.

                        Разнообразный и богатый опыт новая модель организационной деятельности требуют от нас анализа
                        новых предложений. С другой стороны реализация намеченных плановых заданий в значительной
                        степени обуславливает создание соответствующий условий активизации. Не следует, однако забывать,
                        что рамки и место обучения кадров позволяет оценить значение модели развития.</p>
                    <div class="comment__item__name-date--wrapper">
                        <p>Александр, 23 года</p>
                        <p> <span> 23 декабря, 2018</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 show-more">
                <form action="">
                    <a href="#" id="pullItems" value="submit">Показать еще</a>
                </form>
            </div>
        </div>
    </div>
</div>


<?php require_once("slider.php"); ?>
<?php require_once("footer.php"); ?>