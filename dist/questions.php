<?php require_once("header.php"); ?>
<?php require_once("breadCrumbs.php"); ?>

<div class="questions">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="questions__title--wrap">
                    <h3 class="questions__title">Вопросы и Ответы</h3>

                    <p class="questions__ask">
                        Задать вопрос
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="accordion--wrapper">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header px-0" id="headingOne">
                                <span>01</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                        data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Как войти в личный кабинет?
                                    </button>
                                </h2>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingTwo">
                                <span>02</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Как подтвердить оплату в личном кабинете?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingThree">
                                <span>03</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        Возврат и обмен
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingFour">
                                <span>04</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Что делать, если пароль утерян?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingFive">
                                <span>05</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        Как скачать каталоги?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingSix">
                                <span>06</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        Как скачать каталоги?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                                <div class="card-header px-0" id="headingSeven">
                                    <span>07</span>
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                            Как скачать каталоги?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                        этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.
    
                                        И заполнить Google форму для возврата средств:
                                        https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit
    
    
                                        Укажите номер Вашего заказа, модель и причину возврата.
                                        Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                        товара, сохранен его товарный вид (бирка, упаковка).
                                        Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                        производственного брака и пересорта.
                                    </div>
                                </div>
                            </div>
                        <div class="card">
                                <div class="card-header px-0" id="headingEight">
                                    <span>08</span>
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                            Как скачать каталоги?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                        этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.
    
                                        И заполнить Google форму для возврата средств:
                                        https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit
    
    
                                        Укажите номер Вашего заказа, модель и причину возврата.
                                        Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                        товара, сохранен его товарный вид (бирка, упаковка).
                                        Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                        производственного брака и пересорта.
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require_once("slider.php"); ?>
<?php require_once("footer.php"); ?>