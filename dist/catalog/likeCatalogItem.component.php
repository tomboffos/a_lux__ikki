

<!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 283.5 283.5" style="enable-background:new 0 0 283.5 283.5;" xml:space="preserve" width="18px" height="16px">
<style type="text/css">
	.st0{fill:#5F5F5F;}
</style>
<path class="st0" d="M247.3,145.5c-34.5,35.3-69.7,70-104.6,104.9c-0.2,0.2-0.5,0.3-0.7,0.5c-0.4,0.2-0.8,0.5-1.2,0.7
	c-0.9-1.3-1.7-2.7-2.8-3.8c-33.1-33.1-66.3-66.1-99.3-99.4c-19.9-20.1-26.5-44.3-18.3-71.3c8.1-26.8,26.7-43.3,54.1-48.7
	c23.8-4.7,44.7,2.1,62.4,18.6c1.5,1.4,2.5,3.4,3.8,5.3c4.5-4,7.6-7.3,11.2-10c21.9-16.4,46-20.1,71.2-9.5
	c24.8,10.4,39,29.7,42.4,56.4C268.4,110.7,262.4,130.1,247.3,145.5z"/>
</svg>
