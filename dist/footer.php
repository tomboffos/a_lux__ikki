<footer>
    <div class="container">
        <div class="footer-wrap">
            <div class="row">
                <div class="col-sm-3">
                    <div class="column">
                        <h3>О КОМПАНИИ</h3>
                        <a href="./about.php">О нас</a>
                        <a href="./comments.php">Отзывы</a>
                        <a href="./shops.php">Магазины</a>
                        <a href="./contacts.php">Контакты</a>
                        <a href="./vacancy.php">Вакансии</a>
                    </div>
                </div>
                <div class="offset-sm-2"></div>
                <div class="col-sm-3">
                    <div class="column">
                        <h3>ПОКУПАТЕЛЯМ</h3>
                        <a href="">Оформление заказа</a>
                        <a href="./delivery.php">Доставка и оплата</a>
                        <a href="./articles.php">Справочник размеров</a>
                        <a href="./exchange.php">Возврат</a>
                        <a href="./questions.php">Вопросы и ответы</a>
                    </div>
                </div>
                <div class="offset-sm-1"></div>
                <div class="col-sm-3">
                    <div class="column footer__feedback">
                        <h3>УЗНАЙТЕ ПЕРВЫМИ О НОВИНКАХ
                            И СКИДКАХ</h3>
                        <input type="text" placeholder="Укажите Е-mail" class="footer__input">
                        <a href="" class="btn__subscribe">ПОДПИСАТЬСЯ</a>
                        <p>
                            Нажимая на кнопку «Подписаться», я соглашаюсь
                            на обработку моих персональных данных
                            и ознакомлен(а) с <a href="" class="footer__link-terms">условиями конфиденциальности</a>.
                        </p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 px-0">
                        <div class="column">
                            <p>(с) IKKI 2019</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row footer__socials">
                            <a href="">
                                <svg id="_16" data-name="16" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 23.64 50.53" width="7" height="15">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: #262626;
                                                fill-rule: evenodd;
                                            }
                                        </style>
                                    </defs>
                                    <title>facebook</title>
                                    <path class="cls-1"
                                        d="M88.94,82.29V78.23a2.12,2.12,0,0,1,2.22-2.41h5.67V67.15H89c-8.69,0-10.64,6.42-10.64,10.59v4.55h-5V92.41h5.1v25.28H88.55V92.41H96l.33-4,.6-6.14Z"
                                        transform="translate(-73.34 -67.15)" />
                                </svg>
                            </a>
                            <a href="">
                                <svg id="_16" data-name="16" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 49.65 49.65" width="14" height="15">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: #262626;
                                                fill-rule: evenodd;
                                            }
                                        </style>
                                    </defs>
                                    <title>instagram</title>
                                    <path class="cls-1"
                                        d="M198.42,92.41a12.68,12.68,0,1,1,12.67,12.69,12.69,12.69,0,0,1-12.67-12.69Zm25.77-24.82H198a11.77,11.77,0,0,0-11.74,11.74v26.18A11.79,11.79,0,0,0,198,117.25h26.18a11.78,11.78,0,0,0,11.74-11.74V79.33a11.76,11.76,0,0,0-11.74-11.74ZM198,70.53h26.18a8.8,8.8,0,0,1,8.8,8.8v26.18a8.81,8.81,0,0,1-8.8,8.8H198a8.81,8.81,0,0,1-8.8-8.8V79.33a8.8,8.8,0,0,1,8.8-8.8Zm28.79,3.7a2.58,2.58,0,1,0,2.58,2.57,2.57,2.57,0,0,0-2.58-2.57Zm.3,18.18a16,16,0,1,0-16,16,16,16,0,0,0,16-16Z"
                                        transform="translate(-186.27 -67.59)" />
                                </svg>
                            </a>
                            <a href="">
                                <svg id="_16" data-name="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.59 37.1"
                                    width="21" height="15">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: #262626;
                                                fill-rule: evenodd;
                                            }
                                        </style>
                                    </defs>
                                    <title>youtube</title>
                                    <path class="cls-1"
                                        d="M613.7,204.9s-.52-3.64-2-5.23a7.24,7.24,0,0,0-5-2.25c-7.08-.52-17.69-.52-17.69-.52h-.06s-10.58,0-17.65.52a7.18,7.18,0,0,0-5.07,2.25c-1.51,1.59-2,5.23-2,5.23a81,81,0,0,0-.52,8.5v4a79.47,79.47,0,0,0,.52,8.5s.52,3.65,2,5.27c1.92,2.11,4.44,2,5.59,2.25,4,.41,17.19.55,17.19.55s10.61-.06,17.69-.55a7.15,7.15,0,0,0,5-2.25c1.51-1.62,2-5.27,2-5.27a84.26,84.26,0,0,0,.49-8.5v-4c0-4.22-.49-8.5-.49-8.5Zm-16.34,10-13.68,7.38V207.45l6.19,3.38Z"
                                        transform="translate(-563.61 -196.9)" />
                                </svg>
                            </a>
                            <a href="">
                                <svg id="_16" data-name="16" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 50.59 50.53" width="14" height="15">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: #262626;
                                                fill-rule: evenodd;
                                            }
                                        </style>
                                    </defs>
                                    <title>whatsup</title>
                                    <path class="cls-1"
                                        d="M487.27,107.72a24.81,24.81,0,0,0-49.58-.46,7.46,7.46,0,0,0-.05,1.07,24.68,24.68,0,0,0,3.56,12.72l-4.49,13.22,13.76-4.36a25,25,0,0,0,12,3,24.72,24.72,0,0,0,24.84-24.62c0-.22,0-.39,0-.61Zm-13.44,5.6c-.63-.3-3.59-1.78-4.14-2s-1-.31-1.4.33a25.7,25.7,0,0,1-1.92,2.33c-.35.44-.71.46-1.28.19a15.26,15.26,0,0,1-4.91-3,17.83,17.83,0,0,1-3.4-4.14c-.33-.66-.06-.93.24-1.29a6.51,6.51,0,0,0,.94-1,1.18,1.18,0,0,0,.22-.27,7,7,0,0,0,.38-.72.92.92,0,0,0-.06-1c-.1-.3-1.34-3.32-1.86-4.49s-1-1-1.34-1-.77,0-1.18,0a2.09,2.09,0,0,0-1.62.76,6.54,6.54,0,0,0-2.17,5.05,6.33,6.33,0,0,0,.36,2,13.64,13.64,0,0,0,2.14,4.19c.3.41,4.19,6.69,10.39,9.11s6.22,1.59,7.32,1.5,3.59-1.42,4.11-2.87A4.84,4.84,0,0,0,475,114a3.21,3.21,0,0,0-1.15-.66Zm-11.38,15.74A20.77,20.77,0,0,1,451,125.63l-8,2.55,2.57-7.71a20.53,20.53,0,0,1-4-12.14,16.64,16.64,0,0,1,.11-2,20.88,20.88,0,0,1,41.56.38,14.89,14.89,0,0,1,.06,1.62,20.84,20.84,0,0,1-20.87,20.73Z"
                                        transform="translate(-436.71 -83.73)" />
                                </svg>
                            </a>
                            <a href="">
                                <svg id="_16" data-name="16" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 40.44 50.51" width="12" height="15">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: #262626;
                                                fill-rule: evenodd;
                                            }
                                        </style>
                                    </defs>
                                    <title>pinterest</title>
                                    <path class="cls-1"
                                        d="M212.29,313.23c-14.2,0-21.41,9.85-21.41,18.1a15.69,15.69,0,0,0,.55,4.36,9.59,9.59,0,0,0,5.59,6.75,1,1,0,0,0,1.48-.72c.17-.52.47-1.78.63-2.33a1.35,1.35,0,0,0-.41-1.62,7.77,7.77,0,0,1-1.64-3,10,10,0,0,1-.36-2.69c0-7.32,5.67-13.9,14.78-13.9,8,0,12.47,4.74,12.47,11.1a25.82,25.82,0,0,1-.55,5.38c-1.26,5.84-4.55,10.06-9,10.06-3.13,0-5.51-2.49-4.74-5.59.43-1.73,1.06-3.54,1.61-5.29a18.48,18.48,0,0,0,1.07-5c0-2.42-1.31-4.36-4.08-4.36-3.21,0-5.79,3.23-5.79,7.51a12,12,0,0,0,.19,2.14,11,11,0,0,0,.77,2.46s-3.29,13.47-3.84,15.83c-1.15,4.71-.16,10.44-.08,11,0,.33.49.41.71.16s4.12-4.91,5.43-9.48c.33-1.32,2.11-8,2.11-8,1,1.95,4.06,3.65,7.32,3.65,7.82,0,13.63-5.68,15.52-13.93a26.28,26.28,0,0,0,.66-6c0-8.64-7.54-16.62-19-16.62Z"
                                        transform="translate(-190.88 -313.23)" />
                                </svg>
                            </a>

                        </div>
                    </div>
                    <div class="col-sm-4"></div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="./js/main.js"></script>

<script>
    var products = [];
    products.push({
        price: 381300,
        amount: 1,
        id: 768
    });

    var totalSum = 0;

    var discount = 0;
</script>

</body>

</html>