const path = require('path');



module.exports = {
  entry: ['./src/js/hoverIntent.js', './src/js/superfish.js', './src/js/supersubs.js', './src/js/main.js'],
  output: {
    filename: 'main.js',
    path: path.join(__dirname, './dist/js')
  },
  devServer: {
    watchContentBase: true,
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|gif|svg|otf)$/i,
        use: [
          {
            loader: 'url-loader'
          },
        ],
      }
    ],
  },
};