import '../css/main.css';
import '../css/superfish.css';
import '../css/superfish-navbar.css';
import Glide from "@glidejs/glide";





jQuery(function () {
    jQuery('#example').superfish({

    });
});
//////GLOBAL

/////  logo href
document.getElementById("ikkiLogo").addEventListener("click", function () {
    console.log(this.baseURI)
    window.location.replace("./index.php");
})


///// INDEX PAGE

if ($("div").is('.index')) {
    new Glide('.glide', {
        type: "carousel",
    }).mount()
}


////QUESTIONS PAGE

if ($("div").is(".questions")) {
    let collapseList = document.querySelectorAll('.btn')
    let collapse = document.querySelectorAll('.btn').forEach((collapse) => {
        collapse.addEventListener('click', () => {
            for (let i = 0; i < collapseList.length; i++) {
                collapseList[i].parentNode.parentNode.classList.remove("highlightNumber");
                collapseList[i].parentNode.parentNode.classList.remove("collapse-sign");
            }
            if (collapse.parentNode.parentNode.parentNode.children[1].classList.contains("show") === false) {
                console.log(collapse.parentNode.parentNode.parentNode.children[1].classList.contains("show"))
                collapse.parentNode.parentNode.classList.add("collapse-sign");
                collapse.parentNode.parentNode.classList.add("highlightNumber");
            } else if (collapse.parentNode.parentNode.parentNode.children[1].classList.contains("show") === true) {
                console.log(collapse.parentNode.parentNode.parentNode.children[1].classList.contains("show"))
                collapse.parentNode.parentNode.classList.remove("collapse-sign");
                collapse.parentNode.parentNode.classList.remove("highlightNumber");
            }
        });
    });
}

///// ARTICLES PAGE
if (window.location.pathname === "/dist/articles.php") {
    document.getElementById("pullItems").addEventListener("click", getItems);
    const id = ''
    function getItems(e) {
        e.preventDefault()

        let xhr = new XMLHttpRequest()

        xhr.open("GET", "articlesItems", true);

        xhr.onload = () => {


            let article = document.querySelector("#NodeParent");

            article.insertAdjacentHTML('beforeend',
                `
<div class="col-sm-12 col-md-6">
    <div class="articles__item" id="articleTemplate${id}">
        <a href="">
            <img src="${article.imageUrl}" alt="${article.imageUrlDescr}">
        </a>
        <span>${article.date}</span>
        <a href="">${article.title}</a>
        <span>${article.text}</span>
    </div>
</div>

`
            );
        }

        xhr.send()
    }
}


/// COMMENTS PAGE
if (window.location.pathname === "/dist/comments.php") {
    console.log('here')
    let starReviewRating = document.getElementById("starRatingComments");
    let starCounter = starReviewRating.children;
    console.log(starReviewRating);




    for (let i = 0; i <= starCounter.length; i++) { // starCounter[i].addEventListener("mouseover", ()=> {
        // })

    }


    //ADD COMMENT BLOCK

    document.getElementById("pullItems").addEventListener("click", getItems);
    const id = ''
    function getItems(e) {
        e.preventDefault()

        let xhr = new XMLHttpRequest()

        xhr.open("GET", "articlesItems", true);

        xhr.onload = () => {


            let article = document.querySelector("#NodeParent");

            article.insertAdjacentHTML('beforeend',
                `
    <div class="col-sm-12 col-md-6">
        <div class="articles__item" id="articleTemplate${id}">
            <a href="">
                <img src="${article.imageUrl}" alt="${article.imageUrlDescr}">
            </a>
            <span>${article.date}</span>
            <a href="">${article.title}</a>
            <span>${article.text}</span>
        </div>
    </div>

    `
            );
        }

        xhr.send()
    }
}

///CATALOG PAGE
if (window.location.pathname === "/dist/catalog.php") {


    document.querySelectorAll(".catalog__body-container__display__item__frame").forEach((favIcon) => {
        favIcon.children[3].addEventListener("click", () => {

            favIcon.children[3].classList.toggle("inFavorites");
        });
    });

    let menuCatalog = document.querySelectorAll(".catalog__btn-link");
    document.querySelectorAll(".catalog__btn-link").forEach((catalogBtnLink) => {
        catalogBtnLink.addEventListener('click', () => {
            for (let i = 0; i < menuCatalog.length; i++) {
                menuCatalog[i].classList.remove("catalog__btn-link--transform");
            }
            catalogBtnLink.classList.toggle("catalog__btn-link--transform");
        })

    });

    document.querySelectorAll(".quantity-target").forEach((quantityDisplay) => {
        quantityDisplay.addEventListener("click", () => {

            for (let i = 0; i < document.querySelectorAll(".quantity-target").length; i++) {
                document.querySelectorAll(".quantity-target")[i].classList.remove("span-active");
            }
            quantityDisplay.classList.toggle("span-active");
        });
    });
    document.querySelectorAll(".catalog__body-container__filter__row__select").forEach((select) => {
        select.addEventListener("click", () => {

            for (let i = 0; i < document.querySelectorAll(".select-active").length; i++) {
                document.querySelectorAll(".select-active")[i].classList.remove("select-active");
            }
            select.classList.toggle("select-active");
        });
    });
    document.querySelectorAll(".catalog__body-container__filter__order__select").forEach((selectOrder) => {
        selectOrder.addEventListener("click", () => {


            selectOrder.classList.toggle("select__order-active");
        });
    });
    /// SELECT 2 AJAX FILTER

    $(document).ready(function () {

        $('#size').select2({
            ajax: {
                url: 'http://ikki:90/dist/catalog.json',
                dataType: 'json'

            },
            minimumResultsForSearch: -1
        });
        $('#material').select2({
            ajax: {
                url: 'http://ikki:90/dist/catalog.json',
                dataType: 'json'

            },
            minimumResultsForSearch: -1
        });
        $('#detail').select2({
            ajax: {
                url: 'http://ikki:90/dist/catalog.json',
                dataType: 'json'

            },
            minimumResultsForSearch: -1
        });
        $('#style').select2({
            ajax: {
                url: 'http://ikki:90/dist/catalog.json',
                dataType: 'json'

            },
            minimumResultsForSearch: -1
        });
        $('#decore').select2({
            ajax: {
                url: 'http://ikki:90/dist/catalog.json',
                dataType: 'json'

            },
            minimumResultsForSearch: -1
        });

        $('#order').select2({
            ajax: {
                url: 'http://ikki:90/dist/catalog.json',
                dataType: 'json'

            },
            minimumResultsForSearch: -1
        });
        const OrderFilter = {
            order: ""
        }
        document.querySelectorAll(".catalog__body-container__filter__order__select").forEach((selectorFilter) => {
            $('#' + selectorFilter.id).on('change', function (e) {
                OrderFilter[selectorFilter.id] = selectorFilter.value;
                console.log(OrderFilter)
            })


        });
        const filters = {
            price: {
                min: "",
                max: ""
            },
            color: [],
            size: "",
            material: "",
            detail: "",
            style: "",
            decore: ""
        }

        document.querySelectorAll(".catalog__body-container__filter__row__select").forEach((selectorFilter) => {
            $('#' + selectorFilter.id).on('change', function (e) {
                filters[selectorFilter.id] = selectorFilter.value;
                console.log(filters)
            })


        });

        /// SELECT 2 AJAX FILTER END!

    });


    let array1 = ['catalog__item-1.png', 'catalog__item-2.png'];

    document.querySelectorAll(" .catalog__body-container__display__item__frame").forEach((selectOrder) => {
        selectOrder.addEventListener("click", () => {

            // catalog__body - container__display__item__frame__1
            // catalog__body - container__display__item__frame__2
            selectOrder.classList.toggle("select__order-active");
        });
    });

    document.querySelector("#advance-search").addEventListener("click", () => {
        document.querySelector("#advance-field").classList.toggle("advance");
        document.querySelector("#advance-search").classList.toggle('rotate');
    })


    //// ITEM SLIDER 
    const mouseOverObject = {
        src: [
            {
                id: 1,
                path: "catalog__item-1.png"
            },
            {
                id: 2,
                path: "catalog__item-2.png"
            },
            {
                id: 3,
                path: "catalog__item-1.png"
            },
            {
                id: 4,
                path: "catalog__item-2.png"
            },
            {
                id: 5,
                path: "catalog__item-1.png"
            }
        ]
    };

    ///SRC ITEM CHANGE

    document.querySelectorAll(".frameMarker").forEach((slider) => {
        slider.addEventListener("mouseover", () => {
            let current = 1;
            let current2 = 2;
            setInterval(() => {
                slider.children[0].children[1].children[1].style.backgroundImage = `url('${(mouseOverObject.src.find((element) => element.id == current).path)}';)`
                current = current + 1;
                slider.children[0].children[0].children[0].style.zIndex = "0";
                slider.children[0].children[0].children[1].style.zIndex = "1";

                if (current == 5) {
                    current = 1;
                }
            }, 2000);
            setInterval(() => {
                slider.children[0].children[0].children[0].style.backgroundImage = `url('${(mouseOverObject.src.find((element) => element.id == current2).path)}';)`
                current2 = current2 + 1;
                slider.children[0].children[0].children[1].style.zIndex = "0";
                slider.children[0].children[0].children[0].style.zIndex = "1";

                if (current2 == 5) {
                    current2 = 2;
                }
            }, 1000);
        })
        slider.addEventListener("mouseout", () => {
            let current = 1;
            let current2 = 2;
            slider.children[0].children[0].children[1].style.backgroundImage = `url('${(mouseOverObject.src.find((element) => element.id == current).path)}';)`
            slider.children[0].children[0].children[0].style.backgroundImage = `url('${(mouseOverObject.src.find((element) => element.id == current2).path)}';)`
            slider.children[0].children[0].children[1].style.zIndex = "0";
            slider.children[0].children[0].children[0].style.zIndex = "1";
        })
    });
}

////CABINET PAGE
if (window.location.pathname === "/dist/cabinet.php") {
    document.querySelectorAll(".address-item")[1].style.height = "0px";
    document.querySelectorAll(".address-item")[1].style.overflow = "hidden";
    document.querySelector(".add-address-btn").addEventListener("click", () => {
        document.querySelectorAll(".address-item")[1].style.height = "100%";
    });
    $(document).ready(function () {
        var $disabledResults = $(".homenumber");
        $disabledResults.select2();
        $('.homenumber').select2({

            allowClear: true,
            minimumResultsForSearch: Infinity
        });

    });
    $(document).ready(function () {
        var $disabledResults = $(".date");
        $disabledResults.select2();
        $('.date').select2({

            allowClear: true,
            minimumResultsForSearch: Infinity
        });

    });
    $(document).ready(function () {
        var $disabledResults = $(".clothing-sizes");
        $disabledResults.select2();
        $('.clothing-sizes').select2({

            allowClear: true,
            minimumResultsForSearch: Infinity
        });

    });
    $(document).ready(function () {
        var $disabledResults = $(".apartment");
        $disabledResults.select2();
        $('.apartment').select2({

            allowClear: true,
            minimumResultsForSearch: Infinity
        });

    });
    $(document).ready(function () {
        var $disabledResults = $(".porch");
        $disabledResults.select2();
        $('.porch').select2({

            allowClear: true,
            minimumResultsForSearch: Infinity
        });

    });
    $(document).ready(function () {
        var $disabledResults = $(".floor");
        $disabledResults.select2();
        $('.floor').select2({

            allowClear: true,
            minimumResultsForSearch: Infinity
        });

    });
}



//// Product-inner 

if ($("div").is(".product-wrapper")) {
    $('.addToBasket').on('click',function() {
        $('.basket-product').toggleClass('show');
        $('body').toggleClass('push');
        $('.user-cabinet__intrface').toggleClass('push');
    });
    $('.close__btn').on('click', function() {
        $('.basket-product').toggleClass('show');
        $('body').toggleClass('push');
        $('.user-cabinet__intrface').toggleClass('push');
    });
    $(document).mouseup(function (e) {
        let catalog = $('.basket-product');
        if (!catalog.is(e.target) && catalog.has(e.target).length === 0) {
            catalog.removeClass('show');
            $('body').removeClass('push');
            $('.user-cabinet__intrface').removeClass('push');
        }
    });
    // if($(".basket-product").is(".show")) {
    //     $("body").on('click', function() {
    //         if(event.target == event.currentTarget) {
    //             $('.basket-product').removeClass('show');
    //             $('body').removeClass('push');
    //         }
    //     });
    // }
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav',
        draggable: false,
        centerMode: false,
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 3000,
        centerMode: false,
        focusOnSelect: true,
        vertical: true,
        draggable: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    vertical: false,
                    arrows: false,
                }
            }
        ]
    });


    new Glide('.glideTogether', {
        type: 'carousel',
        startAt: 0,
        perView: 4
    }).mount();

    new Glide('.glideSimilar', {
        type: 'carousel',
        startAt: 0,
        perView: 5
    }).mount();

    document.querySelectorAll(".catalog__body-container__display__item__frame").forEach((favIcon) => {
        favIcon.children[3].addEventListener("click", () => {

            favIcon.children[3].classList.toggle("inFavorites");
        });
    });

}


//// BASKET

if (window.location.pathname === "/dist/basket.php") {

    function refreshTotalSum() {
        totalSum = 0;
        for (i = 0; i < products.length; i++) {
            totalSum = totalSum + products[i].price * products[i].amount;
        }
        totalSum = totalSum * (100 - discount) / 100;
        console.log(totalSum);
        document.getElementById('FinalResult').innerHTML = totalSum + ' тг';
        document.getElementById('FormSum').value = totalSum;
    }
    $(document).ready(function () {
        refreshTotalSum();
    });

    function getPromo() {
        if ($('#promo').val() != "") {
            document.getElementById('waitGIF').style.display = 'block';
            $.get('/promo/' + $('#promo').val()).success(function (data) {
                if (parseInt(data) != 0) {
                    if (data == 'no') {
                        document.getElementById('waitGIF').style.display = 'none';
                        Swal.fire({
                            text: "Промокод не найден",
                            type: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#ea893a'
                        })
                    }
                    else {
                        discount = parseInt(data);
                        document.getElementById('discount').innerHTML = discount + '%';
                        document.getElementById('waitGIF').style.display = 'none';
                        refreshTotalSum();
                    }
                }
            });
        }
    };
    function eraseCookie(id) {
        document.cookie = 'product' + id + '=; Max-Age=-99999999;';
        var index = getIndexByValue(products, id);
        products[index].amount = 0;
        refreshTotalSum();
        $('#productString' + id).remove()
    }
    function eraseProducts() {
        for (i = 0; i < products.length; i++) {
            for (i = 0; i < products.length; i++) {
                document.cookie = 'product' + products[i].id + '=; Max-Age=-99999999;';
                products[i].amount = 0;
                $('#productString' + products[i].id).remove()
            }
            refreshTotalSum();
            document.getElementById('finalResult').innerHTML = '0 тг';
        }
    }
    function getIndexByValue(arr, value) {
        for (var i = 0, iLen = arr.length; i < iLen; i++) {
            if (arr[i].id == value) return i;
        }
    }
    function plusAmount(id) {
        var obj = document.getElementById('inputAmount' + id)
        obj.stepUp()
        var index = getIndexByValue(products, id);
        document.getElementById('result' + id).innerHTML = products[index].price * obj.value + 'тг';
        products[index].amount = products[index].amount + 1;
        refreshTotalSum();
        setCookie('product' + id, obj.value, 7)
    }

    function minusAmount(id) {
        var obj = document.getElementById('inputAmount' + id)
        if (obj.value > 1) {
            obj.stepDown()
            var index = getIndexByValue(products, id);
            document.getElementById('result' + id).innerHTML = products[index].price * obj.value + 'тг';
            products[index].amount = products[index].amount - 1;
            refreshTotalSum();
            setCookie('product' + id, obj.value, 7)
        }
    }

}


if (!$("div").is(".catalog") && !$("div").is(".product-wrapper")) {
    new Glide('.glide_sub', {
        type: "carousel",
        perView: 4,
        gap: 32,
        autoplay: 3000,
        breakpoints: {
            900: {
                perView: 3,
            },
            600: {
                perView: 2,
            }
        }
    }).mount()
}


const axios = require('axios');

function send() {

}
